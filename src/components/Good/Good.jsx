import React, { Component } from 'react';
import styles from './Good.module.scss';
import PropTypes from 'prop-types';

import { Button } from '../../UI/Button/Button';
import { Modal } from '../../UI/Modal/Modal';

export class Good extends Component {
  constructor(props) {
    super(props);
    this.item = props.item;
    this.state = {
      addToBasket: false,
      goodsStarId: false,
      goodsBasketId: false,
    };
  }

  componentDidMount() {
    const propsValue = ['goodsStarId', 'goodsBasketId'];

    propsValue.map((name) => {
      const getStorageValue = localStorage.getItem(name);
      if (getStorageValue) {
        const splitValue = getStorageValue.split(',');
        const isGood = splitValue.find((id) => this.item.id === Number(id));
        if (isGood) {
          this.setState({ [name]: true });
        }
      }
    });
  }

  render() {
    const { id, name, price, imgUrl, article, color } = this.item;
    return (
      <>
        <div className={styles.GoodContainer}>
          <div className={styles.Good}>
            <div className={styles.body}>
              <div className={styles.img}>
                <img src={`./img/${imgUrl}`} alt="" />
              </div>
              <div className={styles.title}>{name}</div>
              <div className={styles.article}>
                <span className={styles.articleText}>Артикуль</span>:&nbsp;
                <span className={styles.articleValue}>{article}</span>
              </div>
              <div className={styles.color}>
                <span className={styles.colorText}>Цвет</span>:&nbsp;
                <span className={styles.colorValue}>{color}</span>
              </div>
            </div>
            <div className={styles.bottom}>
              <div className={styles.actions}>
                <span className={styles.basket}>
                  {!this.state.goodsBasketId ? (
                    <i
                      className="fa-solid fa-circle-plus"
                      onClick={() => {
                        this.setState({ addToBasket: true });
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-basket-shopping"
                      onClick={() => {
                        this.setState({ goodsBasketId: false });
                        this.props.removeStorageValue('goodsBasketId', id);
                      }}
                    ></i>
                  )}
                </span>
                <span className={styles.star}>
                  {!this.state.goodsStarId ? (
                    <i
                      className="fa-regular fa-star"
                      onClick={() => {
                        this.setState({ goodsStarId: true });
                        this.props.addStorageValue('goodsStarId', id);
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-star"
                      onClick={() => {
                        this.setState({ goodsStarId: false });
                        this.props.removeStorageValue('goodsStarId', id);
                      }}
                    ></i>
                  )}
                </span>
              </div>
              <div className={styles.price}>
                <span className={styles.sum}>{price}</span>
                <span className={styles.currency}> ₴</span>
              </div>
            </div>
          </div>
        </div>
        {this.state.addToBasket && (
          <Modal
            className={'red'}
            setVisible={() => this.setState({ addToBasket: false })}
            header={<>Хотите добавить товар в корзину ?</>}
            closeButton={true}
            text={
              <div className={styles.modalAddBasket}>
                <div className={styles.title}>{name}</div>
                <div className={styles.price}>
                  <span className={styles.sum}>{price}</span>
                  <span className={styles.currency}> ₴</span>
                </div>
              </div>
            }
            action={
              <>
                <Button
                  className={'red'}
                  text="Ok"
                  backgroundColor="#b3382c"
                  onClick={() => {
                    this.setState({ goodsBasketId: true });

                    this.props.addStorageValue('goodsBasketId', id);
                    this.setState({ addToBasket: false });
                  }}
                />
                <Button
                  className={'red'}
                  text="Cancel"
                  backgroundColor="#b3382c"
                  onClick={() => this.setState({ addToBasket: false })}
                />
              </>
            }
          />
        )}
      </>
    );
  }
}

Good.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,

  addStorageValue: PropTypes.func.isRequired,
  removeStorageValue: PropTypes.func.isRequired,
};

Good.defaultProps = {
  item: {
    name: '',
    price: 0,
    imgUrl: '',
    article: '',
    color: '',
  },
};
